#include "Queen.h"

#include "Bishop.h"
#include "Rook.h"

Queen::Queen()
	: PiceControl()
{
}

Queen::Queen(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

Queen::~Queen()
{
}

bool Queen::checkMove(const Point& toGo)
{

	Rook r(this->_soldierSign, this->_placeOnBoard, this->_place);
	Bishop b(this->_soldierSign, this->_placeOnBoard, this->_place);

	return r.checkMove(toGo) || b.checkMove(toGo);
}

vector<Point> Queen::piceWay(const Point& toGo)
{
	vector<Point> blocksWay;

	if ((this->_placeOnBoard.GetX() == toGo.GetX()) || (this->_placeOnBoard.GetY() == toGo.GetY()))
	{
		Rook r(this->_soldierSign, this->_placeOnBoard, this->_place);
		return r.piceWay(toGo);
	}

	else
	{
		Bishop b(this->_soldierSign, this->_placeOnBoard, this->_place);
		return b.piceWay(toGo);
	}



	return blocksWay;
}