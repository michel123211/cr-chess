#pragma once

#include <iostream>

#include "PiceControl.h"
#include "Rook.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Knight.h"
#include <map>
#include "Point.h"

#include "Board.h"
#include "MoveExeption.h"



#define MOVE_NUMBER_0 0
#define MOVE_NUMBER_1 1
#define MOVE_NUMBER_2 2
#define MOVE_NUMBER_3 3
#define MOVE_NUMBER_4 4
#define MOVE_NUMBER_5 5
#define MOVE_NUMBER_6 6
#define MOVE_NUMBER_7 7
#define MOVE_NUMBER_8 8


using std::map;
using std::string;

class MainGame : public Board
{

public:
	MainGame(const char player);
	void GameControl(string msgFromGame);
	map<string, PiceControl*>* getAllPices();
	~MainGame();


	bool stepOver(vector<Point> blocksWay, char soldierSign);

	void deletePice(string key);
	void addPice(string oldKey, string newKey);
	void SetPlayer(const char player);


	bool canEatKing(bool whiteOrBlack); // ���� ���� �� �� ���� ����� �� ����
	//white = true   islower()
	//black = false   !islower()

	//���� ��� ����, ���� ���(����� �� ������
	//���� ��� ��� ���� ���(����� �� �������

	/*
	 for (map<string, PiceControl*>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
	*/

private:
	char _player;
	// ���� ����� ����� ���� ���� ������

	Board* b = new Board();

	

	//const char& soldierSign, const Point& placeOnBoard, const string& place
	Rook* R1 = new Rook('R', convertString("a8"), "a8"); 
	Knight* K1 = new Knight('K', convertString("b8"), "b8");
	Bishop* B1 = new Bishop('B', convertString("c8"), "c8");
	King* K = new King('K', convertString("d8"), "d8");
	Queen* Q = new Queen('Q', convertString("e8"), "e8");
	Bishop* B2 = new Bishop('B', convertString("f8"), "f8");
	Knight* K2 = new Knight('K', convertString("g8"), "g8");
	Rook* R2 = new Rook('R', convertString("h8"), "h8");

	Pawn* P1 = new Pawn('P', convertString("a7"), "a7");
	Pawn* P2 = new Pawn('P', convertString("b7"), "b7");
	Pawn* P3 = new Pawn('P', convertString("c7"), "c7");
	Pawn* P4 = new Pawn('P', convertString("d7"), "d7");
	Pawn* P5 = new Pawn('P', convertString("e7"), "e7");
	Pawn* P6 = new Pawn('P', convertString("f7"), "f7");
	Pawn* P7 = new Pawn('P', convertString("g7"), "g7");
	Pawn* P8 = new Pawn('P', convertString("h7"), "h7");





	Rook* r1 = new Rook('r', convertString("a1"), "a1");
	Knight* k1 = new Knight('k', convertString("b1"), "b1");
	Bishop* b1 = new Bishop('b', convertString("c1"), "c1");
	King* k = new King('k', convertString("d1"), "d1");
	Queen* q = new Queen('q', convertString("e1"), "e1");
	Bishop* b2 = new Bishop('b', convertString("f1"), "f1");
	Knight* k2 = new Knight('k', convertString("g1"), "g1");
	Rook* r2 = new Rook('r', convertString("h1"), "h1");

	Pawn* p1 = new Pawn('p', convertString("a2"), "a2");
	Pawn* p2 = new Pawn('p', convertString("b2"), "b2");
	Pawn* p3 = new Pawn('p', convertString("c2"), "c2");
	Pawn* p4 = new Pawn('p', convertString("d2"), "d2");
	Pawn* p5 = new Pawn('p', convertString("e2"), "e2");
	Pawn* p6 = new Pawn('p', convertString("f2"), "f2");
	Pawn* p7 = new Pawn('p', convertString("g2"), "g2");
	Pawn* p8 = new Pawn('p', convertString("h2"), "h2");

	map<string, PiceControl*> allPices = 
	{
		{ "a8", R1 }, { "b8", K1 }, { "c8", B1 }, { "d8", K }, { "e8", Q }, { "f8", B2 }, { "g8", K2 }, { "h8", R2 },
		{ "a7", P1 }, { "b7", P2 }, { "c7", P3 }, { "d7", P4 }, { "e7", P5 }, { "f7", P6 }, { "g7", P7 }, { "h7", P8 },

		{ "a2", p1 }, { "b2", p2 }, { "c2", p3 }, { "d2", p4 }, { "e2", p5 }, { "f2", p6 }, { "g2", p7 }, { "h2", p8 },
		{ "a1", r1 }, { "b1", k1 }, { "c1", b1 }, { "d1", k }, { "e1", q }, { "f1", b2 }, { "g1", k2 }, { "h1", r2 }
	};



	string whiteKingKey = "d1";
	string blackKingKey = "d8";
};