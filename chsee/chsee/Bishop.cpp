#include "Bishop.h"
#include <math.h>

Bishop::Bishop()
	: PiceControl()
{
}

Bishop::Bishop(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

Bishop::~Bishop()
{
}

bool Bishop::checkMove(const Point& toGo)
{
	if ((this->_placeOnBoard.GetX() == 0 || this->_placeOnBoard.GetX() == 7) && (toGo.GetX() == 0 || toGo.GetX() == 7))
		return false;

	if ((this->_placeOnBoard.GetX() + this->_placeOnBoard.GetY()) == (toGo.GetX() + toGo.GetY()))
		return true;
	else if (this->_placeOnBoard.GetX() < this->_placeOnBoard.GetY()) // (3, 4)
	{
		return (this->_placeOnBoard.GetY() - this->_placeOnBoard.GetX()) == (toGo.GetY() - toGo.GetX());
	}
	else
	{
		return (this->_placeOnBoard.GetX() - this->_placeOnBoard.GetY()) == (toGo.GetX() - toGo.GetY());
	}
	/*return
		(this->_placeOnBoard.GetX() + this->_placeOnBoard.GetY()) == (toGo.GetX() + toGo.GetY())
		||
		abs(this->_placeOnBoard.GetX() - this->_placeOnBoard.GetY()) == abs(toGo.GetX() - toGo.GetY());*/
}

vector<Point> Bishop::piceWay(const Point& toGo)
{
	vector<Point> blocksWay;

	int xToSet = this->_placeOnBoard.GetX();
	int yToSet = this->_placeOnBoard.GetY();

	if ((this->_placeOnBoard.GetX() + this->_placeOnBoard.GetY()) == (toGo.GetX() + toGo.GetY())) // -> /
	{
		if (this->_placeOnBoard.GetX() < toGo.GetX()) // bottom (3, 3) -> (4, 2)
		{
			++xToSet;
			--yToSet;
			while (xToSet <= toGo.GetX()) // 3 <= 4
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				++xToSet;
				--yToSet;
			}
		}

		else // top (3, 3) -> (2, 4)   3 > 2
		{
			--xToSet;
			++yToSet;
			while (xToSet >= toGo.GetX()) // 3 >= 2
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				--xToSet;
				++yToSet;
			}
		}
	}
	else // -> /* \ */  
	{
		if (this->_placeOnBoard.GetX() < toGo.GetX()) // top  (3, 3) -> (4, 4)
		{
			xToSet++;
			yToSet++;
			while (xToSet <= toGo.GetX()) 
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				++xToSet;
				++yToSet;
			}
		}

		else // bottom 
		{
			--xToSet;
			--yToSet;
			while (xToSet >= toGo.GetX())
			{
				blocksWay.push_back(Point(xToSet, yToSet));
				--xToSet;
				--yToSet;
			}
		}
	}
	return blocksWay;
}