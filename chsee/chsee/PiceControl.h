#pragma once

#include <iostream>
#include "Point.h"


#include <vector>
using std::vector;

using std::cout;
using std::cin;
using std::endl;
using std::string;

class PiceControl
{
public:
	PiceControl();
	PiceControl(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~PiceControl();


	virtual char GetSoldierSign();
	virtual void SetSoldierSign(const char& soldierSign);

	virtual Point GetPlaceOnBoard();
	virtual void SetPlaceOnBoard(const Point& placeOnBoard);
	 
	virtual string GetPlace();
	virtual void SetPlace(const string& place);


	virtual bool checkMove(const Point& toGo) = 0;
	virtual vector<Point> piceWay(const Point& toGo) = 0;


	virtual bool allWaysPice() = 0;

	void SetThreat(const bool threat);
	bool GetThreat() const;



protected:
	bool _threat;

	char _soldierSign;
	Point _placeOnBoard;
	string _place;
};