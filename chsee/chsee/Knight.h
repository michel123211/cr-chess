#pragma once

#include "PiceControl.h"

class Knight : public PiceControl
{
public:
	Knight();
	Knight(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~Knight();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);

};