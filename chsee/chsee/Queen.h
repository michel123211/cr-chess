#pragma once

#include "PiceControl.h"
#include "Rook.h"
#include "Bishop.h"

class Queen : public PiceControl//, Rook, Bishop
{
public:
	Queen();
	Queen(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~Queen();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);



};