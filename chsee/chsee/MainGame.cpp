#include "MainGame.h"

MainGame::MainGame(const char player)
{
	this->_player = player;
}

MainGame::~MainGame()
{
}


map<string, PiceControl*>* MainGame::getAllPices()
{
	return &(this->allPices);
}


void MainGame::SetPlayer(const char player)
{
	this->_player = player;
}

void MainGame::deletePice(string key)
{
	this->allPices.erase(key);
}

void MainGame::addPice(string oldKey, string newKey)
{
	PiceControl* temp(this->allPices[oldKey]);

	temp->SetPlace(newKey);
	temp->SetPlaceOnBoard(convertString(newKey));


	//deletePice(oldKey);
	this->allPices.insert(std::pair<string, PiceControl*>(newKey, temp));
}


void MainGame::GameControl(string msgFromGame)
{
	string picePlace;
	string piceToGo;

	picePlace += msgFromGame[0];
	picePlace += msgFromGame[1];

	piceToGo += msgFromGame[2];
	piceToGo += msgFromGame[3];


	Point picePlacePoint(convertString(picePlace));
	Point piceToGoPoint(convertString(piceToGo));


	if (board[picePlacePoint.GetX()][picePlacePoint.GetY()] == '#')
		throw MoveExeption(MOVE_NUMBER_2);

	//else if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] == '#')
	//	throw MoveExeption(MOVE_NUMBER_3);

	else if ((this->_player == 'W') && (!islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]))
		|| (this->_player == 'B') && (islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()])))
		throw MoveExeption(MOVE_NUMBER_2);

	else if ((picePlacePoint.GetX() < 0 || picePlacePoint.GetX() > LEN_OF_BOARD)
		|| (picePlacePoint.GetY() < 0 || picePlacePoint.GetY() > LEN_OF_BOARD)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetX() > LEN_OF_BOARD)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetY() > LEN_OF_BOARD))
		throw MoveExeption(MOVE_NUMBER_5);

	else if (picePlacePoint == piceToGoPoint)
		throw MoveExeption(MOVE_NUMBER_7);






	else if ((this->allPices)[picePlace]->checkMove(piceToGoPoint))
	{
		/*if (islower(this->allPices[picePlace]->GetSoldierSign()) &&
			!this->allPices[whiteKingKey]->GetThreat() || !islower(this->allPices[picePlace]->GetSoldierSign()) &&
			!this->allPices[blackKingKey]->GetThreat()) // move white*/
		
		if (stepOver((this->allPices)[picePlace]->piceWay(piceToGoPoint), this->allPices[picePlace]->GetSoldierSign()))
		{
			throw MoveExeption(MOVE_NUMBER_6);
		}

		else
		{
			if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#')
			{
				if ((islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()]))
					|| (!islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && !islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()])))
				{
					throw MoveExeption(MOVE_NUMBER_3);
				}

				else
					deletePice(piceToGo);
			}

			if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#'
				&& ((this->allPices)[picePlace]->GetSoldierSign() == 'p' || (this->allPices)[picePlace]->GetSoldierSign() == 'P'))
			{
				throw MoveExeption(MOVE_NUMBER_6);
			}

			else
			{
				board[picePlacePoint.GetX()][picePlacePoint.GetY()] = '#';
				board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
				addPice(picePlace, piceToGo);

				if (islower((this->allPices)[piceToGo]->GetSoldierSign())) // true = white
				{
					if((this->allPices)[piceToGo]->GetSoldierSign() == 'k')
					{
						whiteKingKey = (this->allPices)[piceToGo]->GetPlace();
					}
					if (canEatKing(false))
					{
						addPice(piceToGo, picePlace);

						board[picePlacePoint.GetX()][picePlacePoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
						board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = '#';

						if ((this->allPices)[piceToGo]->GetSoldierSign() == 'k')
						{
							whiteKingKey = (this->allPices)[piceToGo]->GetPlace();
						}

						throw MoveExeption(MOVE_NUMBER_4);
					}
				}
				else // false = black
				{
					if ((this->allPices)[piceToGo]->GetSoldierSign() == 'K')
					{
						blackKingKey = (this->allPices)[piceToGo]->GetPlace();
					}
					if (canEatKing(true))
					{
						cout << "CHESSSSSSSSSS" << endl;
						addPice(piceToGo, picePlace);

						board[picePlacePoint.GetX()][picePlacePoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
						board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = '#';

						if ((this->allPices)[piceToGo]->GetSoldierSign() == 'K')
						{
							blackKingKey = (this->allPices)[piceToGo]->GetPlace();
						}

						throw MoveExeption(MOVE_NUMBER_4);
					}
				}

				if (canEatKing(islower((this->allPices)[piceToGo]->GetSoldierSign())))
				{
					//addPice(piceToGo, picePlace);   //
					if (islower((this->allPices)[piceToGo]->GetSoldierSign())) // true 
					{
						this->allPices[blackKingKey]->SetThreat(true);
					}
					else
					{
						this->allPices[whiteKingKey]->SetThreat(true);
					}

					throw MoveExeption(MOVE_NUMBER_1);
				}

				else
				{
					deletePice(picePlace);
					//addPice(piceToGo, picePlace);

					//board[picePlacePoint.GetX()][picePlacePoint.GetY()] = '#';
					//board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
					//addPice(picePlace, piceToGo);



					/*if ((this->allPices)[piceToGo]->GetSoldierSign() == 'k') // white
					{
						whiteKingKey = (this->allPices)[piceToGo]->GetPlace();
					}

					else if ((this->allPices)[piceToGo]->GetSoldierSign() == 'K') // white
					{
						blackKingKey = (this->allPices)[piceToGo]->GetPlace();
					}*/

					if (islower((this->allPices)[piceToGo]->GetSoldierSign())) // true 
					{
						this->allPices[blackKingKey]->SetThreat(false);
					}
					else
					{
						this->allPices[whiteKingKey]->SetThreat(false);
					}

					throw MoveExeption(MOVE_NUMBER_0);
				}

			}
		}
			
	}

	else if ((this->allPices)[picePlace]->GetSoldierSign() == 'p' || (this->allPices)[picePlace]->GetSoldierSign() == 'P')
	{
		//Pawn p(
			//this->allPices)[picePlace]; // .validEat(piceToGoPoint); //////////////////////////////////////////////
		Pawn p((this->allPices)[picePlace]->GetSoldierSign(), (this->allPices)[picePlace]->GetPlaceOnBoard(), (this->allPices)[picePlace]->GetPlace());
		if (p.validEat(piceToGoPoint) && board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#')
		{
			deletePice(piceToGo);
			board[picePlacePoint.GetX()][picePlacePoint.GetY()] = '#';
			board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
			addPice(picePlace, piceToGo);
			throw MoveExeption(MOVE_NUMBER_0);
		}
		else
			throw MoveExeption(MOVE_NUMBER_6);
	}

	else
		throw MoveExeption(MOVE_NUMBER_6);
}


bool MainGame::stepOver(vector<Point> blocksWay, char soldierSign) // ������ �� ����
{
	if (('k' == soldierSign) || ('K' == soldierSign))
		return false;
	else
	{
		//true ������
		//false �� ������
		for (int i = 0; i < blocksWay.size() - 1; i++) // WAS: for (int i = 0; i < blocksWay.size(); i++)
		{
			if (board[blocksWay[i].GetX()][blocksWay[i].GetY()] != '#')
			{
				return true;
			}
		}
		return false;
	}
}



bool MainGame::canEatKing(bool whiteOrBlack) 
// true = white   false = black
{
	//���� ��� ����, ���� ���(����� �� ������
	//���� ��� ��� ���� ���(����� �� �������
	Point* toSend = new Point();


	if (whiteOrBlack)
		// true = check the white   if bl
		//point ��� ���
		toSend = &(this->allPices[blackKingKey]->GetPlaceOnBoard());

	else
		//point ��� ����
		toSend = &(this->allPices[whiteKingKey]->GetPlaceOnBoard());

	

	for (map<string, PiceControl*>::iterator pice = this->allPices.begin(); pice != this->allPices.end(); ++pice)
	{
		if (islower(pice->second->GetSoldierSign()) && (whiteOrBlack) || !islower(pice->second->GetSoldierSign()) && !(whiteOrBlack)) // islower('r') == true   was ==
			if (pice->second->checkMove(*toSend)) //����� ����� ����   �� ��� ���� ����� ����
				if (!stepOver(pice->second->piceWay(*toSend), pice->second->GetSoldierSign())) // true = chess
					return true;
	}
				
			
	return false;
	
}