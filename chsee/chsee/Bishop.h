#pragma once

#include "PiceControl.h"

class Bishop : public PiceControl
{
public:
	Bishop();
	Bishop(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~Bishop();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);


};