#pragma once

#include "PiceControl.h"

class Rook : public PiceControl
{
public:
	Rook();
	Rook(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~Rook();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);
	virtual bool allWaysPice() = 0;


};