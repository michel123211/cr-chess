/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>

#include <map>

#include "Board.h"
#include "Regular.h"

#include "PiceControl.h"
#include "MainGame.h"
#include "MoveExeption.h"

#include "Point.h"

#include <typeinfo>

using std::cout;
using std::endl;
using std::string;
//using std::map;

void main()
{
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans < "0" || ans > "1")		
		{
			cout << "Ilegal input!!!" << endl;
		}

		else if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string


	MainGame* mG = new MainGame('B');
	mG->print();
	bool playerChange = false;
	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	//Board* b = new Board();
	int r = 0;

	

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		// YOUR CODE

		//mG->printBoard();
		
		//b->print();
		strcpy_s(msgToGraphics, "YOUR CODE"); // msgToGraphics should contain the result of the operation
		try
		{
			mG->GameControl(msgFromGraphics);
		}
		catch (MoveExeption & e)
		{
			if (e.what()[0] == '1' || e.what()[0] == '0')
				playerChange = !playerChange;

			playerChange ? mG->SetPlayer('W') : mG->SetPlayer('B');

			p.sendMessageToGraphics(e.what());
		}
		mG->print();
		printDetails(msgFromGraphics); //////////////////////////

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}