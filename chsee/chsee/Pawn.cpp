#include "Pawn.h"
#include "Board.h"

Pawn::Pawn()
	: PiceControl()
{
}

Pawn::Pawn(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: PiceControl(soldierSign, placeOnBoard, place)
{
}

Pawn::~Pawn()
{
}

bool Pawn::validEat(const Point& toGo)
{
	//return (b.getLetter(toGo.GetX(), toGo.GetY()) == '#');
	if (((this->_placeOnBoard.GetX() - 1) == (toGo.GetX()) && (this->_placeOnBoard.GetY() + 1) == (toGo.GetY())
		|| (this->_placeOnBoard.GetX() - 1) == (toGo.GetX()) && (this->_placeOnBoard.GetY() - 1) == (toGo.GetY()))

		||
		((this->_placeOnBoard.GetX() + 1) == (toGo.GetX()) && (this->_placeOnBoard.GetY() - 1) == (toGo.GetY())
			|| (this->_placeOnBoard.GetX() + 1) == (toGo.GetX()) && (this->_placeOnBoard.GetY() + 1) == (toGo.GetY())))
	{
		numsOfMoves++;
		return true;
	}
	return false;
}

bool Pawn::checkMove(const Point& toGo)
{
	//���� ����� �� ���� ������ ������ �� �� ������ ����� �����
	if (numsOfMoves == 0)
	{
		if (islower(this->_soldierSign))
		{
			if ((this->_placeOnBoard.GetX() - 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY()) == (toGo.GetY())
				|| (this->_placeOnBoard.GetX() - 1) == (toGo.GetX())
					&& (this->_placeOnBoard.GetY()) == (toGo.GetY()))

			{
				numsOfMoves++;
				return true;
			}
			return false;
		}

		else
		{
			if ((this->_placeOnBoard.GetX() + 2) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY()) == (toGo.GetY())
				|| (this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				    && (this->_placeOnBoard.GetY()) == (toGo.GetY()))
			{
				numsOfMoves++;
				return true;
			}
			return false;
		}
	}

	else
	{
		if (islower(this->_soldierSign))
		{
			return
				(this->_placeOnBoard.GetX() - 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY()) == (toGo.GetY());
		}

		else
		{
			return
				(this->_placeOnBoard.GetX() + 1) == (toGo.GetX())
				&& (this->_placeOnBoard.GetY()) == (toGo.GetY());
		}
	}
}

vector<Point> Pawn::piceWay(const Point& toGo)
{
	vector<Point> blocksWay;

	int xToSet = this->_placeOnBoard.GetX();
	int yToSet = this->_placeOnBoard.GetY();

	if (islower(this->_soldierSign))
	{
		--xToSet;
		while (xToSet >= toGo.GetX())
		{
			blocksWay.push_back(Point(xToSet, yToSet));
			--xToSet;
		}
	}

	else
	{
		++xToSet;
		while (xToSet <= toGo.GetX())
		{
			blocksWay.push_back(Point(xToSet, yToSet));
			++xToSet;
		}
	}

	return blocksWay;
}