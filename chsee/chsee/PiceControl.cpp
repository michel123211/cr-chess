#include "PiceControl.h"

PiceControl::PiceControl()
{
}

PiceControl::PiceControl(const char& soldierSign, const Point& placeOnBoard, const string& place)
	: _soldierSign(soldierSign), _placeOnBoard(placeOnBoard), _place(place)
{
}

PiceControl::~PiceControl()
{
}



char PiceControl::GetSoldierSign()
{
	return this->_soldierSign;
}

void PiceControl::SetSoldierSign(const char& soldierSign)
{
	this->_soldierSign = soldierSign;
}


////////////////////////////////////////////
void PiceControl::SetThreat(const bool threat)
{
	this->_threat = threat;
}

bool PiceControl::GetThreat() const
{
	return this->_threat;
}
/////////////////////////////////////////////


string PiceControl::GetPlace()
{
	return this->_place;
}

void PiceControl::SetPlace(const string& place)
{
	this->_place = place;
}



Point PiceControl::GetPlaceOnBoard()
{
	return this->_placeOnBoard;
}

void PiceControl::SetPlaceOnBoard(const Point& placeOnBoard)
{
	this->_placeOnBoard = placeOnBoard;
}

/*
	virtual bool checkMove(const Point& toGo) const = 0;
	virtual Point* piceWay() = 0;*/