#include "Point.h"
#include <string>
#include <iostream> 

using namespace std;


Point::Point()
{
}

Point::Point(int n, int t)
	: _x(n), _y(t)
{}

Point::~Point()
{}

int Point::GetX() const
{
	return this->_x;
}

int Point::GetY() const
{
	return this->_y;
}

void Point::SetX(const int n)
{
	this->_x = n;
}

void Point::SetY(const int t)
{
	this->_y = t;
}





Point::Point(const Point& other)
{
	this->_x = other.GetX();
	this->_y = other.GetY();
}

Point& Point::operator=(const Point& other)
{
	if (this == &other)
	{
		return *this;
	}
	this->_x = other.GetX();
	this->_y = other.GetY();

	return *this;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other.GetX();
	this->_y += other.GetY();
	return *this;
}

bool Point::operator==(const Point& other)
{
	return (this->_x == other.GetX()) && (this->_y == other.GetY());
}




Point /*Point::*/convertString(const std::string place)
{
	char x = place[1];
	char y = place[0];

	int x_int = 0;
	int y_int = 0;

	y = tolower(y);
	y_int = int(y) - 97;

	x_int = x - '0';
	x_int = 8 - (1 * x_int);

	//std::cout << "(" << x_int << ", " << y_int << ")";


	Point p(x_int, y_int);
	return p;
}


void printDetails(const std::string msgFromMsg)
{
	/*
Receives 5 bytes; Message: "h2h4"
Move from: 6, 7 To 4, 7
Sends 2 bytes; Message: "0"
*/
	std::string picePlace;
	picePlace += msgFromMsg[0];
	picePlace += msgFromMsg[1];

	std::string piceMove;
	piceMove += msgFromMsg[2];
	piceMove += msgFromMsg[3];



	Point picePointPlace(convertString(picePlace));
	Point PicePointMove(convertString(piceMove));

	std::cout << "Move from " <<
		picePointPlace.GetX() << ", " << picePointPlace.GetY() << " To " <<
		PicePointMove.GetX() << ", " << PicePointMove.GetY() << std::endl;
}